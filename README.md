### DESCRIPTION
This is test task. I use here Django 1.11, django rest framework and react. 
PEP8 and JS styleguides were not strictly followed on purpose.  

### DEPENDENCIES
Redux was not used here for sakes of simplicity. For same reasons we use sqlite as database.
You need to have node.js, python3 and virtualenv installed

### SETUP 
To setup project locally clone it from git, then create virtualenv(with python3) and install django dependencies:

(assuming you are in root of project)
```
pip install -r requirements.txt
```
migrate database and create superuser for access to django admin
```
cd backend 
python manage.py migrate
python manage.py createsuperuser
```
start backend server

```
python manage.py runserver
```

Open project in another terminal and install react dependencies:

(assuming you are in root of project)
```
cd frontend
npm install
```

and start frontend server:

```
npm start
```
