from rest_framework import serializers
from .models import PatientProfile, DoctorProfile


class DoctorDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = DoctorProfile
        fields = (
            'full_name',
            'office_phone',
        )


class PatientListSerializer(serializers.HyperlinkedModelSerializer):
    medical_conditions = serializers.StringRelatedField(many=True)

    class Meta:
        model = PatientProfile
        fields = (
                    'url',
                    'id',
                    'full_name',
                    'gender',
                    'date_of_birth',
                    'medical_conditions',
                )
        extra_kwargs = {
            'url': {'view_name': 'api:patient-detail', 'lookup_field': 'pk'},
        }


class PatientDetailSerializer(serializers.HyperlinkedModelSerializer):
    medical_conditions = serializers.StringRelatedField(many=True)
    doctor = DoctorDetailSerializer()

    class Meta:
        model = PatientProfile
        fields = (
            'full_name',
            'gender',
            'date_of_birth',
            'doctor',
            'medical_conditions',
            'telephone',
            'cellphone',
            'address',
            'email',
            'url'
        )

        extra_kwargs = {
            'url': {'view_name': 'api:invite-patient', 'lookup_field': 'pk'},
        }


class ConcretePatientSerializer(serializers.ModelSerializer):
    medical_conditions = serializers.StringRelatedField(many=True)
    doctor = DoctorDetailSerializer()

    class Meta:
        model = PatientProfile
        fields = (
            'full_name',
            'gender',
            'date_of_birth',
            'doctor',
            'medical_conditions',
            'telephone',
            'cellphone',
            'address',
            'email',
            'password_change_required',
        )
