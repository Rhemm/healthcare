from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^patients/$', views.PatientList.as_view()),
    url(r'^patients/(?P<pk>[0-9]+)/$', views.PatientDetail.as_view(), name='patient-detail'),
    url(r'^invite-patient/(?P<pk>[0-9]+)/$', views.InvitePatient.as_view(), name='invite-patient'),
    url(r'^concrete-patient/$', views.ConcretePatientDetail.as_view(), name='concrete-patient'),
    url(r'^change-password/$', views.ChangePassword.as_view(), name='change-password'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
