from django.shortcuts import render, get_object_or_404
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.views import APIView, Response
from .models import PatientProfile, DoctorProfile, User
from .serializers import PatientListSerializer, PatientDetailSerializer, ConcretePatientSerializer
from .permissions import IsDoctorPermission, IsOwnerOrDoctorPermission
from .exceptions import UserAlreadyExists
from .utils import send_ivitation_email
from rest_framework.permissions import IsAuthenticated
from django.core.exceptions import ObjectDoesNotExist


class PatientList(ListAPIView):
    """
    Gives list of all patients
    """
    queryset = PatientProfile.objects.all()
    serializer_class = PatientListSerializer
    permission_classes = [IsDoctorPermission]


class PatientDetail(RetrieveAPIView):
    """
    Gives full information about patient defined by url
    """
    serializer_class = PatientDetailSerializer
    permission_classes = [IsOwnerOrDoctorPermission]

    def get_queryset(self):
        patient_query = PatientProfile.objects.filter(pk=self.kwargs['pk'])
        return patient_query


class ConcretePatientDetail(APIView):
    """
    Gives full information about patient to themself
    """
    permission_classes = [IsAuthenticated]

    def get(self, request):
        patient = get_object_or_404(PatientProfile, user=self.request.user)
        serializer = ConcretePatientSerializer(patient)
        return Response(serializer.data)


class InvitePatient(APIView):
    """
    Creates user, link it to patientprofile and send email
    """
    permission_classes = [IsDoctorPermission]

    def get_object(self):
        obj = get_object_or_404(PatientProfile, pk=self.kwargs['pk'])
        return obj

    def post(self, request, **kwargs):
        patient = self.get_object()
        try:
            username, password = PatientProfile.register_patient(patient, request.user)
            if send_ivitation_email(username, password, patient.email, patient.full_name):
                return Response({'message': 'user is successfully invited'})
            else:
                return Response({'message': 'email is not sent'}, status=400)
        except UserAlreadyExists:
            return Response({"message": "user is already invited"}, status=400)


class ChangePassword(APIView):
    """
    Changes password for user
    """
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        user = request.user
        success = False
        try: 
            patient = PatientProfile.objects.get(user=user)
            success = patient.change_password(data['old_password'], data['new_password'])
        except ObjectDoesNotExist:
            if user.check_password(data['old_password']):
                user.set_password(data['new_password'])
                user.save()
                success = True
        if success:
            return Response({'message': 'password was changed successfully'})
        return Response({'message': 'current password was wrong'}, status=400)
