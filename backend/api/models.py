import random 
import string
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from .exceptions import UserAlreadyExists
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


# We put creating of a token here to ensure so it will be imported by django on startup
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


phone_regex = RegexValidator(regex=r'^\d{9,15}$',
                             message="Phone number must be entered in the format: '999999999'."
                                     "Up to 15 digits allowed(min 9).")


class MedicalCondition(models.Model):
    """
    Model representing medical condition.
    """
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class DoctorProfile(models.Model):
    """
    Model representing doctor.
    """
    user = models.OneToOneField(User)
    office_phone = models.CharField(max_length=15, validators=[phone_regex])

    @property
    def full_name(self):
        return 'Dr. {}'.format(self.user.get_full_name())

    def __str__(self):
        return self.full_name


class PatientProfile(models.Model):
    """
    Model representing patient.
    """
    user = models.OneToOneField(User, blank=True, null=True)
    doctor = models.ForeignKey(DoctorProfile, blank=True, null=True)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    medical_conditions = models.ManyToManyField(MedicalCondition,
                                                related_name='medical_condition')
    telephone = models.CharField(max_length=15, validators=[phone_regex])
    cellphone = models.CharField(max_length=15, validators=[phone_regex])
    address = models.CharField(max_length=255)
    # Controversial topic in year 2018. Better to leave as charfield
    gender = models.CharField(max_length=64)
    date_of_birth = models.DateField()
    email = models.EmailField()
    password_change_required = models.BooleanField(default=True)

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __str__(self):
        return self.full_name

    @classmethod
    def register_patient(cls, patient, doctor):
        if not User.objects.filter(patientprofile=patient):
            while True:
                username = ''.join(random.choice(string.ascii_letters) for _ in range(10))
                if not User.objects.filter(username=username).count():
                    break
            user = User.objects.create(first_name=patient.first_name, last_name=patient.last_name,
                                        username=username, email=patient.email)
            password = User.objects.make_random_password()
            user.set_password(password)
            user.save()
            patient.user = user
            patient.doctor = DoctorProfile.objects.get(user=doctor)
            patient.save()
            return (username, password)
        raise UserAlreadyExists

    def change_password(self, old_password, new_password):
        user = self.user
        if user.check_password(old_password):
            user.set_password(new_password)
            self.password_change_required = False
            user.save()
            self.save()
            return True
        return False
