from django.core.mail import send_mail
from django.conf import settings


def send_ivitation_email(username, password, email, full_name):
    return send_mail(
        'healthcare invite',
        'Dear {}.Thank you for agreeing to participate in our Patient Care Management Program. This is a positive first step towards managing your health.'
        'To complete your enrollment, please sign in using the details below:'
        'Login: {} Password: {}'.format(full_name, username, password),
        settings.FROM_EMAIL,
        [email]
    )
