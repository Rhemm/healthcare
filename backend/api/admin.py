from django.contrib import admin
from .models import DoctorProfile, PatientProfile, MedicalCondition


@admin.register(DoctorProfile)
class DoctorAdmin(admin.ModelAdmin):
    pass


@admin.register(PatientProfile)
class PatientAdmin(admin.ModelAdmin):
    pass


@admin.register(MedicalCondition)
class MedicalConditionAdmin(admin.ModelAdmin):
    pass
