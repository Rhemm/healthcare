from rest_framework import permissions


class IsDoctorPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if hasattr(request.user, 'doctorprofile'):
            return True
        return False 


class IsOwnerOrDoctorPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user or hasattr(request.user, 'doctorprofile')
