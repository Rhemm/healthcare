import React, { Component } from 'react';
import axios from 'axios';
import Navigation from './Navigation';


class PasswordChangeForm extends Component {
    constructor(props) {
      super(props);
      this.state = {oldPass: '', newPass: '', confirmPass: ''};
  
      this.handleOldPassChange = this.handleOldPassChange.bind(this);
      this.handleNewPassChange = this.handleNewPassChange.bind(this);
      this.handleConfirmPassChange = this.handleConfirmPassChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleOldPassChange(event) {
      this.setState({oldPass: event.target.value});
    }
  
    handleNewPassChange(event) {
      this.setState({newPass: event.target.value});
    }
  
    handleConfirmPassChange(event) {
      this.setState({confirmPass: event.target.value});
    }
  
    handleSubmit(event) {
      if (this.state.newPass === this.state.confirmPass){  
        axios.post('http://localhost:8000/api/change-password/', 
        {old_password: this.state.oldPass, new_password: this.state.newPass}, {headers: {
          "Authorization": ("Token " + sessionStorage.getItem('token'))
        }}).then(
          result => {
            alert('Your password was chaged succesfully');
            this.setState({oldPass: '', newPass: '', confirmPass: ''})
          }
        ).catch(error => {
          alert('current password was wrong');
        }); 
        
      } else {
        alert("Your new password and password confirmation doesn't match");
        this.setState({newPass: '', confirmPass: ''})
      }
      event.preventDefault();
    }
  
    
    render() {
      return (
        <div>
          <Navigation/>
          <h2>You have change your password to proceed</h2> 
          <form onSubmit={this.handleSubmit}>
            <label>
              Current password:
              <input type="password" value={this.state.oldPass} onChange={this.handleOldPassChange} />
            </label>
            <label>
              New password:
              <input type="password" value={this.state.newPass} onChange={this.handleNewPassChange} />
            </label>
            <label>
              Confirm password:
              <input type="password" value={this.state.confirmPass} onChange={this.handleConfirmPassChange} />
            </label>
            <input type="submit" value="Submit" />
          </form>
        </div>
      );
    }
  }

export default PasswordChangeForm;