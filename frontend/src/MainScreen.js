import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios';
import PatientInfo from './PatientInfo';
import PatientList from './PatientList';
import Navigation from './Navigation';


class MainScreen extends Component {
    constructor(props) {
      super(props);
      this.state = {
        patientData: {},
        doctorData: {}
      };
    }
  
    componentDidMount() {
        axios.all([
          axios.get('http://localhost:8000/api/patients/', {headers: {
            "Authorization": ("Token " + sessionStorage.getItem('token'))
          }}).catch(error => false),
          axios.get('http://localhost:8000/api/concrete-patient/', {headers: {
            "Authorization": ("Token " + sessionStorage.getItem('token'))
          }}
          ).catch(error => false)
        ]).then(axios.spread((doctorRes, patientRes) => {
            sessionStorage.setItem('isDoctor', doctorRes.data)
            this.setState({doctorData: doctorRes.data, patientData: patientRes.data})
          })
        )
    }
  
    render () {
      if (this.state.doctorData) {
        return (
          <div>
            <Navigation />
            <PatientList {...this.state.doctorData}/>
          </div>
        ) 
      } else {
        if (this.state.patientData.password_change_required) {
          return (
            <Redirect to={{pathname: "/change-password"}}/>
          )
        } else {
          return (
            <div>
            <Navigation />
            <PatientInfo {...this.state.patientData}/>
          </div>
        )
        }
      }
    }
  }

export default MainScreen;