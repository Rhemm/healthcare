import React from 'react';
import {Link} from 'react-router-dom';

const PatientInfo = (props, children) => {
    const {url, id, password_change_required, ...renderList} = props;
    const link = id ? <Link to={`/info/${id}`}>See profile</Link> : ''


    const display = Object.keys(renderList).map(key => {
        if (key === 'doctor') {
            const doctorList = renderList[key]
            if (doctorList) {
                return (
                    <li>{key}:
                    <ul>
                        {Object.keys(doctorList).map(key =>
                            <li>
                                {key}: {doctorList[key]}
                            </li>
                        )}
                    </ul>
                </li>
                )
            }
            return (
                null
            )
        } else if (key === 'medical_conditions') {
            return (
                <li>{key}: 
                    <ul>
                        {renderList[key].map(item =>
                            <li>
                                {item}
                            </li>
                        )}
                    </ul>
                </li> 
            )
        } else {
            return (
                <li>{key}: {renderList[key]}</li> 
            )
        }
    }
    )

    return (
        <div>
            <ul className='patient-info'>      
                {display}
                {link}
            </ul>
        </div>
    );
}

export default PatientInfo;