import React from 'react';
import PatientInfo from './PatientInfo';

const PatientList = (props) => (
    Object.values(props).map(item => (
        <PatientInfo {...item} />
    ))
)

export default PatientList;