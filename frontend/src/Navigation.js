import React from 'react';
import {Link} from 'react-router-dom';


const Navigation = () => (
    <div>
      <Link to='/'>Home</Link>
      <Link to='/logout'>Logout</Link>
      <Link to='/change-password'>Change password</Link>
    </div>
)

export default Navigation;