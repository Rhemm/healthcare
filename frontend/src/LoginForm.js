import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios';


class LoginForm extends Component {
    constructor(props) {
      super(props);
      this.state = {nameValue: '', passValue: ''};
  
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handlePassChange = this.handlePassChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleNameChange(event) {
      this.setState({nameValue: event.target.value});
    }
  
    handlePassChange(event) {
      this.setState({passValue: event.target.value});
    }
  
    handleSubmit(event) {
      axios.post('http://localhost:8000/api-token-auth/', {username: this.state.nameValue, password: this.state.passValue}).then(
        result => {
          sessionStorage.setItem('token', result.data.token);
          this.setState({nameValue: '', passValue: ''})
        }
      ).catch(error => {
        alert('wrong credentials');
      }); 
  
      event.preventDefault();
    }
  
    
    render() {
      if (sessionStorage.token) {
        return (
          <Redirect to={{ pathname: "/" }} />
        )
      }
  
      return (
        <form onSubmit={this.handleSubmit}>
          <label>
            Username:
            <input type="text" value={this.state.nameValue} onChange={this.handleNameChange} />
          </label>
          <label>
            Password:
            <input type="password" value={this.state.passValue} onChange={this.handlePassChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      );
    }
}

export default LoginForm;