import React, { Component } from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import MainScreen from './MainScreen';
import DetailInfo from './DetailInfo';
import PasswordChangeForm from './PasswordChangeForm';
import LoginForm from './LoginForm';
import Logout from './Logout';
import './App.css';


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      sessionStorage.token ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const Main = () => (
  <Switch>
    <PrivateRoute exact path='/' component={MainScreen}/>
    <PrivateRoute path='/info/:id' component={DetailInfo}/>
    <PrivateRoute path='/change-password' component={PasswordChangeForm}/>
    <Route path='/login' component={LoginForm}/>
    <Route path='/logout' component={Logout}/>
  </Switch>
);


class App extends Component {

  render() {
    return (
      <Main/>
    );
  }
}

export default App;
