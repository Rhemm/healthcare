import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios';
import PatientInfo from './PatientInfo';
import Navigation from './Navigation';


class DetailInfo extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data: {}
      };
  
      this.handleOnClick = this.handleOnClick.bind(this);
    }
  
    handleOnClick = () => {
      // Important note here. For some reason axios call then when retrieves 400 error
      // didn't want to waste too much time on library bug
  
      // axios.interceptors.response.use((response) => {
      //   // return response;
      // }, (error) => {
      //         return Promise.reject(error)
      //     }
      // );
      
      axios.post(this.state.data.url, {}, {headers: {
            "Authorization": ("Token " + sessionStorage.getItem('token'))
      }}).catch(error => alert(error.response.data.message)).then(alert('user is succesfully invited(it is only true if you see no error after this message)'));
    }
  
    componentDidMount() {
      axios.get(`http://localhost:8000/api/patients/${this.props.match.params.id}/`, {headers: {
        "Authorization": ("Token " + sessionStorage.getItem('token'))}}).then(result => {
          this.setState({data: result.data}) 
        }
      ).catch(error => false)
    }
  
    render() {
      if (Boolean(sessionStorage.isDoctor) === true) {
        return (
          <div>
            <Navigation/>
            <PatientInfo {...this.state.data}/>
            <button onClick={this.handleOnClick}>Invite patient</button>
          </div>
        )
      }
      return (
        <Redirect to={{ pathname: "/" }} />
      )
    }
  }

export default DetailInfo;